import FWCore.ParameterSet.Config as cms

bfragWgtProducer = cms.EDProducer('BFragmentationWeightProducer',
                                  filePath_frag = cms.FileInPath('TopQuarkAnalysis/BFragmentationAnalyzer/data/bfragweights_CP5.root'),
                                  filePath_dec  = cms.FileInPath('TopQuarkAnalysis/BFragmentationAnalyzer/data/bdecayweights.root')
                                  )
